using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Portal.DAL;

namespace Portal.BLL.Base
{
	public class UserBase
	{
		protected static Portal.DAL.UserDal dal = new Portal.DAL.UserDal();

		public System.Int32 Id		{ get ; set; }

		public System.String Username		{ get ; set; }

		public System.String Password		{ get ; set; }

		public System.Int32 UserRole		{ get ; set; }


		public  Int32 InsertUser()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Username", Username);
			lstItems.Add("@Password", Password);
			lstItems.Add("@UserRole", UserRole.ToString(CultureInfo.InvariantCulture));

			return dal.InsertUser(lstItems);
		}

		public  Int32 UpdateUser()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString());
			lstItems.Add("@Username", Username);
			lstItems.Add("@Password", Password);
			lstItems.Add("@UserRole", UserRole.ToString());

			return dal.UpdateUser(lstItems);
		}

		public  Int32 DeleteUserById(Int32 Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id);

			return dal.DeleteUserById(lstItems);
		}

		public List<User> GetAllUser()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllUser(lstItems);
			List<User> UserList = new List<User>();
			foreach (DataRow dr in dt.Rows)
			{
				UserList.Add(GetObject(dr));
			}
			return UserList;
		}

		public User  GetUserById(Int32 _Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", _Id);

			DataTable dt = dal.GetUserById(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  User GetObject(DataRow dr)
		{

			User objUser = new User();
			objUser.Id = (dr["Id"] == DBNull.Value) ? 0 : (Int32)dr["Id"];
			objUser.Username = (dr["Username"] == DBNull.Value) ? "" : (String)dr["Username"];
			objUser.Password = (dr["Password"] == DBNull.Value) ? "" : (String)dr["Password"];
			objUser.UserRole = (dr["UserRole"] == DBNull.Value) ? 0 : (Int32)dr["UserRole"];

			return objUser;
		}
	}
}
