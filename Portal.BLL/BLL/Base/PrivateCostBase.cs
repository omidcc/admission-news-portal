using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Portal.DAL;

namespace Portal.BLL.Base
{
	public class PrivateCostBase
	{
		protected static Portal.DAL.PrivateCostDal dal = new Portal.DAL.PrivateCostDal();

		public System.Int32 Id		{ get ; set; }

		public System.Int32 UnivId		{ get ; set; }

		public System.String Cost		{ get ; set; }


		public  Int32 InsertPrivateCost()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@UnivId", UnivId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@Cost", Cost);

			return dal.InsertPrivateCost(lstItems);
		}

		public  Int32 UpdatePrivateCost()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString());
			lstItems.Add("@UnivId", UnivId.ToString());
			lstItems.Add("@Cost", Cost);

			return dal.UpdatePrivateCost(lstItems);
		}

		public  Int32 DeletePrivateCostById(Int32 Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id);

			return dal.DeletePrivateCostById(lstItems);
		}

		public List<PrivateCost> GetAllPrivateCost()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllPrivateCost(lstItems);
			List<PrivateCost> PrivateCostList = new List<PrivateCost>();
			foreach (DataRow dr in dt.Rows)
			{
				PrivateCostList.Add(GetObject(dr));
			}
			return PrivateCostList;
		}

		public PrivateCost  GetPrivateCostById(Int32 _Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", _Id);

			DataTable dt = dal.GetPrivateCostById(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  PrivateCost GetObject(DataRow dr)
		{

			PrivateCost objPrivateCost = new PrivateCost();
			objPrivateCost.Id = (dr["Id"] == DBNull.Value) ? 0 : (Int32)dr["Id"];
			objPrivateCost.UnivId = (dr["UnivId"] == DBNull.Value) ? 0 : (Int32)dr["UnivId"];
			objPrivateCost.Cost = (dr["Cost"] == DBNull.Value) ? "" : (String)dr["Cost"];

			return objPrivateCost;
		}
	}
}
