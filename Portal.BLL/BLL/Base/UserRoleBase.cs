using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Portal.DAL;

namespace Portal.BLL.Base
{
	public class UserRoleBase
	{
		protected static Portal.DAL.UserRoleDal dal = new Portal.DAL.UserRoleDal();

		public System.Int32 Id		{ get ; set; }

		public System.String Role		{ get ; set; }


		public  Int32 InsertUserRole()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Role", Role);

			return dal.InsertUserRole(lstItems);
		}

		public  Int32 UpdateUserRole()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString());
			lstItems.Add("@Role", Role);

			return dal.UpdateUserRole(lstItems);
		}

		public  Int32 DeleteUserRoleById(Int32 Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id);

			return dal.DeleteUserRoleById(lstItems);
		}

		public List<UserRole> GetAllUserRole()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllUserRole(lstItems);
			List<UserRole> UserRoleList = new List<UserRole>();
			foreach (DataRow dr in dt.Rows)
			{
				UserRoleList.Add(GetObject(dr));
			}
			return UserRoleList;
		}

		public UserRole  GetUserRoleById(Int32 _Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", _Id);

			DataTable dt = dal.GetUserRoleById(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  UserRole GetObject(DataRow dr)
		{

			UserRole objUserRole = new UserRole();
			objUserRole.Id = (dr["Id"] == DBNull.Value) ? 0 : (Int32)dr["Id"];
			objUserRole.Role = (dr["Role"] == DBNull.Value) ? "" : (String)dr["Role"];

			return objUserRole;
		}
	}
}
