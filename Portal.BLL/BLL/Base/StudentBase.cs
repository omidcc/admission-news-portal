using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Portal.DAL;

namespace Portal.BLL.Base
{
	public class StudentBase
	{
		protected static Portal.DAL.StudentDal dal = new Portal.DAL.StudentDal();

		public System.Int32 Id		{ get ; set; }

		public System.String Name		{ get ; set; }

		public System.String fName		{ get ; set; }

		public System.String mName		{ get ; set; }

		public System.String Dob		{ get ; set; }

		public System.String HSCReg		{ get ; set; }

		public System.String HSCRoll		{ get ; set; }

		public System.Decimal HSCCgpa		{ get ; set; }

		public System.Int32 HSCPassingYear		{ get ; set; }

		public System.String HSCGroup		{ get ; set; }

		public System.String HSCInstitution		{ get ; set; }

		public System.String HSCBoard		{ get ; set; }

		public System.String SSCReg		{ get ; set; }

		public System.String SSCRoll		{ get ; set; }

		public System.Decimal SSCCgpa		{ get ; set; }

		public System.String SSCPassingYear		{ get ; set; }

		public System.String SSCGroup		{ get ; set; }

		public System.String SSCInstitution		{ get ; set; }

		public System.String SSCBoard		{ get ; set; }


		public  Int32 InsertStudent()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Name", Name);
			lstItems.Add("@fName", fName);
			lstItems.Add("@mName", mName);
			lstItems.Add("@Dob", Dob);
			lstItems.Add("@HSCReg", HSCReg);
			lstItems.Add("@HSCRoll", HSCRoll);
			lstItems.Add("@HSCCgpa", HSCCgpa.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@HSCPassingYear", HSCPassingYear.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@HSCGroup", HSCGroup);
			lstItems.Add("@HSCInstitution", HSCInstitution);
			lstItems.Add("@HSCBoard", HSCBoard);
			lstItems.Add("@SSCReg", SSCReg);
			lstItems.Add("@SSCRoll", SSCRoll);
			lstItems.Add("@SSCCgpa", SSCCgpa.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@SSCPassingYear", SSCPassingYear);
			lstItems.Add("@SSCGroup", SSCGroup);
			lstItems.Add("@SSCInstitution", SSCInstitution);
			lstItems.Add("@SSCBoard", SSCBoard);

			return dal.InsertStudent(lstItems);
		}

		public  Int32 UpdateStudent()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString());
			lstItems.Add("@Name", Name);
			lstItems.Add("@fName", fName);
			lstItems.Add("@mName", mName);
			lstItems.Add("@Dob", Dob);
			lstItems.Add("@HSCReg", HSCReg);
			lstItems.Add("@HSCRoll", HSCRoll);
			lstItems.Add("@HSCCgpa", HSCCgpa.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@HSCPassingYear", HSCPassingYear.ToString());
			lstItems.Add("@HSCGroup", HSCGroup);
			lstItems.Add("@HSCInstitution", HSCInstitution);
			lstItems.Add("@HSCBoard", HSCBoard);
			lstItems.Add("@SSCReg", SSCReg);
			lstItems.Add("@SSCRoll", SSCRoll);
			lstItems.Add("@SSCCgpa", SSCCgpa.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@SSCPassingYear", SSCPassingYear);
			lstItems.Add("@SSCGroup", SSCGroup);
			lstItems.Add("@SSCInstitution", SSCInstitution);
			lstItems.Add("@SSCBoard", SSCBoard);

			return dal.UpdateStudent(lstItems);
		}

		public  Int32 DeleteStudentById(Int32 Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id);

			return dal.DeleteStudentById(lstItems);
		}

		public List<Student> GetAllStudent()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllStudent(lstItems);
			List<Student> StudentList = new List<Student>();
			foreach (DataRow dr in dt.Rows)
			{
				StudentList.Add(GetObject(dr));
			}
			return StudentList;
		}

		public Student  GetStudentById(Int32 _Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", _Id);

			DataTable dt = dal.GetStudentById(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  Student GetObject(DataRow dr)
		{

			Student objStudent = new Student();
			objStudent.Id = (dr["Id"] == DBNull.Value) ? 0 : (Int32)dr["Id"];
			objStudent.Name = (dr["Name"] == DBNull.Value) ? "" : (String)dr["Name"];
			objStudent.fName = (dr["fName"] == DBNull.Value) ? "" : (String)dr["fName"];
			objStudent.mName = (dr["mName"] == DBNull.Value) ? "" : (String)dr["mName"];
			objStudent.Dob = (dr["Dob"] == DBNull.Value) ? "" : (String)dr["Dob"].ToString();
			objStudent.HSCReg = (dr["HSCReg"] == DBNull.Value) ? "" : (String)dr["HSCReg"];
			objStudent.HSCRoll = (dr["HSCRoll"] == DBNull.Value) ? "" : (String)dr["HSCRoll"];
			objStudent.HSCCgpa = (dr["HSCCgpa"] == DBNull.Value) ? 0 : (Decimal)dr["HSCCgpa"];
			objStudent.HSCPassingYear = (dr["HSCPassingYear"] == DBNull.Value) ? 0 : (Int32)dr["HSCPassingYear"];
			objStudent.HSCGroup = (dr["HSCGroup"] == DBNull.Value) ? "" : (String)dr["HSCGroup"];
			objStudent.HSCInstitution = (dr["HSCInstitution"] == DBNull.Value) ? "" : (String)dr["HSCInstitution"];
			objStudent.HSCBoard = (dr["HSCBoard"] == DBNull.Value) ? "" : (String)dr["HSCBoard"];
			objStudent.SSCReg = (dr["SSCReg"] == DBNull.Value) ? "" : (String)dr["SSCReg"];
			objStudent.SSCRoll = (dr["SSCRoll"] == DBNull.Value) ? "" : (String)dr["SSCRoll"];
			objStudent.SSCCgpa = (dr["SSCCgpa"] == DBNull.Value) ? 0 : (Decimal)dr["SSCCgpa"];
			objStudent.SSCPassingYear = (dr["SSCPassingYear"] == DBNull.Value) ? "" : (String)dr["SSCPassingYear"];
			objStudent.SSCGroup = (dr["SSCGroup"] == DBNull.Value) ? "" : (String)dr["SSCGroup"];
			objStudent.SSCInstitution = (dr["SSCInstitution"] == DBNull.Value) ? "" : (String)dr["SSCInstitution"];
			objStudent.SSCBoard = (dr["SSCBoard"] == DBNull.Value) ? "" : (String)dr["SSCBoard"];

			return objStudent;
		}
	}
}
