using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Portal.DAL;

namespace Portal.BLL.Base
{
	public class ApplyBase
	{
		protected static Portal.DAL.ApplyDal dal = new Portal.DAL.ApplyDal();

		public System.Int32 Id		{ get ; set; }

		public System.Int32 StudentId		{ get ; set; }

		public System.Int32 UniversityId		{ get ; set; }

		public System.String AppliedDate		{ get ; set; }


		public  Int32 InsertApply()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@StudentId", StudentId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@UniversityId", UniversityId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@AppliedDate", AppliedDate);

			return dal.InsertApply(lstItems);
		}

		public  Int32 UpdateApply()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString());
			lstItems.Add("@StudentId", StudentId.ToString());
			lstItems.Add("@UniversityId", UniversityId.ToString());
			lstItems.Add("@AppliedDate", AppliedDate);

			return dal.UpdateApply(lstItems);
		}

		public  Int32 DeleteApplyById(Int32 Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id);

			return dal.DeleteApplyById(lstItems);
		}

		public List<Apply> GetAllApply()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllApply(lstItems);
			List<Apply> ApplyList = new List<Apply>();
			foreach (DataRow dr in dt.Rows)
			{
				ApplyList.Add(GetObject(dr));
			}
			return ApplyList;
		}

		public Apply  GetApplyById(Int32 _Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", _Id);

			DataTable dt = dal.GetApplyById(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  Apply GetObject(DataRow dr)
		{

			Apply objApply = new Apply();
			objApply.Id = (dr["Id"] == DBNull.Value) ? 0 : (Int32)dr["Id"];
			objApply.StudentId = (dr["StudentId"] == DBNull.Value) ? 0 : (Int32)dr["StudentId"];
			objApply.UniversityId = (dr["UniversityId"] == DBNull.Value) ? 0 : (Int32)dr["UniversityId"];
			objApply.AppliedDate = (dr["AppliedDate"] == DBNull.Value) ? "" : (String)dr["AppliedDate"];

			return objApply;
		}
	}
}
