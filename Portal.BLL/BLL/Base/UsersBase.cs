using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Portal.DAL;

namespace Portal.BLL.Base
{
	public class UsersBase
	{
		protected static Portal.DAL.UsersDal dal = new Portal.DAL.UsersDal();

		public System.Int32 Id		{ get ; set; }

		public System.String Username		{ get ; set; }

		public System.String Password		{ get ; set; }

		public System.Int32 UserRole		{ get ; set; }


		public  Int32 InsertUsers()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Username", Username);
			lstItems.Add("@Password", Password);
			lstItems.Add("@UserRole", UserRole.ToString(CultureInfo.InvariantCulture));

			return dal.InsertUsers(lstItems);
		}

		public  Int32 UpdateUsers()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString());
			lstItems.Add("@Username", Username);
			lstItems.Add("@Password", Password);
			lstItems.Add("@UserRole", UserRole.ToString());

			return dal.UpdateUsers(lstItems);
		}

		public  Int32 DeleteUsersById(Int32 Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id);

			return dal.DeleteUsersById(lstItems);
		}

		public List<Users> GetAllUsers()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllUsers(lstItems);
			List<Users> UsersList = new List<Users>();
			foreach (DataRow dr in dt.Rows)
			{
				UsersList.Add(GetObject(dr));
			}
			return UsersList;
		}

		public Users  GetUsersById(Int32 _Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", _Id);

			DataTable dt = dal.GetUsersById(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  Users GetObject(DataRow dr)
		{

			Users objUsers = new Users();
			objUsers.Id = (dr["Id"] == DBNull.Value) ? 0 : (Int32)dr["Id"];
			objUsers.Username = (dr["Username"] == DBNull.Value) ? "" : (String)dr["Username"];
			objUsers.Password = (dr["Password"] == DBNull.Value) ? "" : (String)dr["Password"];
			objUsers.UserRole = (dr["UserRole"] == DBNull.Value) ? 0 : (Int32)dr["UserRole"];

			return objUsers;
		}
	}
}
