using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Portal.DAL;

namespace Portal.BLL.Base
{
	public class UniversityBase
	{
		protected static Portal.DAL.UniversityDal dal = new Portal.DAL.UniversityDal();

		public System.Int32 Id		{ get ; set; }

		public System.String UnivName		{ get ; set; }

		public System.Boolean IsPublic		{ get ; set; }

		public System.String WebLink		{ get ; set; }

		public System.Decimal MinTotal		{ get ; set; }

		public System.Decimal MaxTotal		{ get ; set; }


		public  Int32 InsertUniversity()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@UnivName", UnivName);
			lstItems.Add("@IsPublic", IsPublic);
			lstItems.Add("@WebLink", WebLink);
			lstItems.Add("@MinTotal", MinTotal.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@MaxTotal", MaxTotal.ToString(CultureInfo.InvariantCulture));

			return dal.InsertUniversity(lstItems);
		}

		public  Int32 UpdateUniversity()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString());
			lstItems.Add("@UnivName", UnivName);
			lstItems.Add("@IsPublic", IsPublic);
			lstItems.Add("@WebLink", WebLink);
			lstItems.Add("@MinTotal", MinTotal.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@MaxTotal", MaxTotal.ToString(CultureInfo.InvariantCulture));

			return dal.UpdateUniversity(lstItems);
		}

		public  Int32 DeleteUniversityById(Int32 Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id);

			return dal.DeleteUniversityById(lstItems);
		}

		public List<University> GetAllUniversity()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllUniversity(lstItems);
			List<University> UniversityList = new List<University>();
			foreach (DataRow dr in dt.Rows)
			{
				UniversityList.Add(GetObject(dr));
			}
			return UniversityList;
		}

		public University  GetUniversityById(Int32 _Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", _Id);

			DataTable dt = dal.GetUniversityById(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  University GetObject(DataRow dr)
		{

			University objUniversity = new University();
			objUniversity.Id = (dr["Id"] == DBNull.Value) ? 0 : (Int32)dr["Id"];
			objUniversity.UnivName = (dr["UnivName"] == DBNull.Value) ? "" : (String)dr["UnivName"];
			objUniversity.IsPublic = (dr["IsPublic"] == DBNull.Value) ? false : (Boolean)dr["IsPublic"];
			objUniversity.WebLink = (dr["WebLink"] == DBNull.Value) ? "" : (String)dr["WebLink"];
			objUniversity.MinTotal = (dr["MinTotal"] == DBNull.Value) ? 0 : (Decimal)dr["MinTotal"];
			objUniversity.MaxTotal = (dr["MaxTotal"] == DBNull.Value) ? 0 : (Decimal)dr["MaxTotal"];

			return objUniversity;
		}
	}
}
