using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using Portal.DAL;

namespace Portal.BLL.Base
{
	public class AdmissionCircularBase
	{
		protected static Portal.DAL.AdmissionCircularDal dal = new Portal.DAL.AdmissionCircularDal();

		public System.Int32 Id		{ get ; set; }

		public System.Int32 UnivId		{ get ; set; }

		public System.String Circular		{ get ; set; }

		public System.Int32 year		{ get ; set; }

		public System.String PostedDate		{ get ; set; }

		public System.String StartDate		{ get ; set; }

		public System.String EndDate		{ get ; set; }


		public  Int32 InsertAdmissionCircular()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@UnivId", UnivId.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@Circular", Circular);
			lstItems.Add("@year", year.ToString(CultureInfo.InvariantCulture));
			lstItems.Add("@PostedDate", PostedDate);
			lstItems.Add("@StartDate", StartDate);
			lstItems.Add("@EndDate", EndDate);

			return dal.InsertAdmissionCircular(lstItems);
		}

		public  Int32 UpdateAdmissionCircular()
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id.ToString());
			lstItems.Add("@UnivId", UnivId.ToString());
			lstItems.Add("@Circular", Circular);
			lstItems.Add("@year", year.ToString());
			lstItems.Add("@PostedDate", PostedDate);
			lstItems.Add("@StartDate", StartDate);
			lstItems.Add("@EndDate", EndDate);

			return dal.UpdateAdmissionCircular(lstItems);
		}

		public  Int32 DeleteAdmissionCircularById(Int32 Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", Id);

			return dal.DeleteAdmissionCircularById(lstItems);
		}

		public List<AdmissionCircular> GetAllAdmissionCircular()
		{
			Hashtable lstItems = new Hashtable();
			DataTable dt = dal.GetAllAdmissionCircular(lstItems);
			List<AdmissionCircular> AdmissionCircularList = new List<AdmissionCircular>();
			foreach (DataRow dr in dt.Rows)
			{
				AdmissionCircularList.Add(GetObject(dr));
			}
			return AdmissionCircularList;
		}

		public AdmissionCircular  GetAdmissionCircularById(Int32 _Id)
		{
			Hashtable lstItems = new Hashtable();
			lstItems.Add("@Id", _Id);

			DataTable dt = dal.GetAdmissionCircularById(lstItems);
			DataRow dr = dt.Rows[0];
			return GetObject(dr);
		}

		protected  AdmissionCircular GetObject(DataRow dr)
		{

			AdmissionCircular objAdmissionCircular = new AdmissionCircular();
			objAdmissionCircular.Id = (dr["Id"] == DBNull.Value) ? 0 : (Int32)dr["Id"];
			objAdmissionCircular.UnivId = (dr["UnivId"] == DBNull.Value) ? 0 : (Int32)dr["UnivId"];
			objAdmissionCircular.Circular = (dr["Circular"] == DBNull.Value) ? "" : (String)dr["Circular"];
			objAdmissionCircular.year = (dr["year"] == DBNull.Value) ? 0 : (Int32)dr["year"];
			objAdmissionCircular.PostedDate = (dr["PostedDate"] == DBNull.Value) ? "" : (String)dr["PostedDate"].ToString();
			objAdmissionCircular.StartDate = (dr["StartDate"] == DBNull.Value) ? "" : (String)dr["StartDate"].ToString();
			objAdmissionCircular.EndDate = (dr["EndDate"] == DBNull.Value) ? "" : (String)dr["EndDate"].ToString();

			return objAdmissionCircular;
		}
	}
}
