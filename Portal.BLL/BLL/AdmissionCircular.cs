using System;
using System.Text;
using System.Data;
using System.Collections;
using Portal.BLL.Base;

namespace Portal.BLL
{
	public class AdmissionCircular : Portal.BLL.Base.AdmissionCircularBase
	{
		private static Portal.DAL.AdmissionCircularDal Dal = new Portal.DAL.AdmissionCircularDal();
		public AdmissionCircular() : base()
		{
		}

	    public int GetMaxId()
	    {
	        return dal.getMaxid();
	    }
	}
}
