using System;
using System.Text;
using System.Data;
using System.Collections;
using Portal.BLL.Base;

namespace Portal.BLL
{
	public class University : Portal.BLL.Base.UniversityBase
	{
		private static Portal.DAL.UniversityDal Dal = new Portal.DAL.UniversityDal();
		public University() : base()
		{
		}

        public int GetMaxId()
        {
            return dal.GetMaxId();
        }
    }
}
