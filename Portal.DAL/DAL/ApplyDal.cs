using System;
using System.Text;
using System.Data;
using System.Collections;
using Portal.DAL.Base;

namespace Portal.DAL
{
	public class ApplyDal : Portal.DAL.Base.ApplyDalBase
	{
		public ApplyDal() : base()
		{
		}

	    public int GetmaxId()
	    {
            try
            {
                int maxId = GetMaximumID("Apply", "Id", 0, "");
                return maxId;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
	    }
	}
}
