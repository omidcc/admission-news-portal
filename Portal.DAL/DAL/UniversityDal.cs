using System;
using System.Text;
using System.Data;
using System.Collections;
using Portal.DAL.Base;

namespace Portal.DAL
{
	public class UniversityDal : Portal.DAL.Base.UniversityDalBase
	{
		public UniversityDal() : base()
		{
		}

        public int GetMaxId()
        {
            try
            {
                int maxId = GetMaximumID("University", "Id", 0, "");
                return maxId;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
