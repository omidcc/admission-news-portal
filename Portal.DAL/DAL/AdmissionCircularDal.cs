using System;
using System.Text;
using System.Data;
using System.Collections;
using Portal.DAL.Base;

namespace Portal.DAL
{
	public class AdmissionCircularDal : Portal.DAL.Base.AdmissionCircularDalBase
	{
		public AdmissionCircularDal() : base()
		{
		}

	    public int getMaxid()
	    {
            try
            {
                int maxId = GetMaximumID("AdmissionCircular", "Id", 0, "");
                return maxId;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
	    }
	}
}
