using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;

namespace Portal.DAL.Base
{
	public class UserDalBase : SqlServerConnection
	{
		public DataTable GetAllUser(Hashtable lstData)
		{
			DataTable dt = new DataTable();
            //try
            //{
				dt = GetDataTable("User", "*", "", lstData);
				return dt;
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
		}

		public DataTable GetUserById(Hashtable lstData)
		{
			string whereCondition = " where User.Id = @Id ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("User", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertUser(Hashtable lstData)
		{
			string sqlQuery ="Insert into User (Username, Password, UserRole) values(@Username, @Password, @UserRole);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateUser(Hashtable lstData)
		{
			string sqlQuery = "Update User set Username = @Username, Password = @Password, UserRole = @UserRole where User.Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteUserById(Hashtable lstData)
		{
			string sqlQuery = "delete from  User where Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
