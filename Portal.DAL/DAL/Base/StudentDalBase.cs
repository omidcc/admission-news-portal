using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;

namespace Portal.DAL.Base
{
	public class StudentDalBase : SqlServerConnection
	{
		public DataTable GetAllStudent(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("Student", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetStudentById(Hashtable lstData)
		{
			string whereCondition = " where Student.Id = @Id ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("Student", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertStudent(Hashtable lstData)
		{
			string sqlQuery ="Insert into Student (Name, fName, mName, Dob, HSCReg, HSCRoll, HSCCgpa, HSCPassingYear, HSCGroup, HSCInstitution, HSCBoard, SSCReg, SSCRoll, SSCCgpa, SSCPassingYear, SSCGroup, SSCInstitution, SSCBoard) values(@Name, @fName, @mName, @Dob, @HSCReg, @HSCRoll, @HSCCgpa, @HSCPassingYear, @HSCGroup, @HSCInstitution, @HSCBoard, @SSCReg, @SSCRoll, @SSCCgpa, @SSCPassingYear, @SSCGroup, @SSCInstitution, @SSCBoard);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateStudent(Hashtable lstData)
		{
			string sqlQuery = "Update Student set Name = @Name, fName = @fName, mName = @mName, Dob = @Dob, HSCReg = @HSCReg, HSCRoll = @HSCRoll, HSCCgpa = @HSCCgpa, HSCPassingYear = @HSCPassingYear, HSCGroup = @HSCGroup, HSCInstitution = @HSCInstitution, HSCBoard = @HSCBoard, SSCReg = @SSCReg, SSCRoll = @SSCRoll, SSCCgpa = @SSCCgpa, SSCPassingYear = @SSCPassingYear, SSCGroup = @SSCGroup, SSCInstitution = @SSCInstitution, SSCBoard = @SSCBoard where Student.Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteStudentById(Hashtable lstData)
		{
			string sqlQuery = "delete from  Student where Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
