using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;

namespace Portal.DAL.Base
{
	public class UsersDalBase : SqlServerConnection
	{
		public DataTable GetAllUsers(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("Users", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetUsersById(Hashtable lstData)
		{
			string whereCondition = " where Users.Id = @Id ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("Users", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertUsers(Hashtable lstData)
		{
			string sqlQuery ="Insert into Users (Username, Password, UserRole) values(@Username, @Password, @UserRole);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateUsers(Hashtable lstData)
		{
			string sqlQuery = "Update Users set Username = @Username, Password = @Password, UserRole = @UserRole where Users.Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteUsersById(Hashtable lstData)
		{
			string sqlQuery = "delete from  Users where Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
