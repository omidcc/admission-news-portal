using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;

namespace Portal.DAL.Base
{
	public class UniversityDalBase : SqlServerConnection
	{
		public DataTable GetAllUniversity(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("University", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetUniversityById(Hashtable lstData)
		{
			string whereCondition = " where University.Id = @Id ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("University", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertUniversity(Hashtable lstData)
		{
			string sqlQuery ="Insert into University (UnivName, IsPublic, WebLink, MinTotal, MaxTotal) values(@UnivName, @IsPublic, @WebLink, @MinTotal, @MaxTotal);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateUniversity(Hashtable lstData)
		{
			string sqlQuery = "Update University set UnivName = @UnivName, IsPublic = @IsPublic, WebLink = @WebLink, MinTotal = @MinTotal, MaxTotal = @MaxTotal where University.Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteUniversityById(Hashtable lstData)
		{
			string sqlQuery = "delete from  University where Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
