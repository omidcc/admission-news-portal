using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;

namespace Portal.DAL.Base
{
	public class PrivateCostDalBase : SqlServerConnection
	{
		public DataTable GetAllPrivateCost(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("PrivateCost", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetPrivateCostById(Hashtable lstData)
		{
			string whereCondition = " where PrivateCost.Id = @Id ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("PrivateCost", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertPrivateCost(Hashtable lstData)
		{
			string sqlQuery ="Insert into PrivateCost (UnivId, Cost) values(@UnivId, @Cost);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdatePrivateCost(Hashtable lstData)
		{
			string sqlQuery = "Update PrivateCost set UnivId = @UnivId, Cost = @Cost where PrivateCost.Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeletePrivateCostById(Hashtable lstData)
		{
			string sqlQuery = "delete from  PrivateCost where Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
