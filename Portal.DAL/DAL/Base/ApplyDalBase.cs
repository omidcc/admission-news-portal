using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;

namespace Portal.DAL.Base
{
	public class ApplyDalBase : SqlServerConnection
	{
		public DataTable GetAllApply(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("Apply", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetApplyById(Hashtable lstData)
		{
			string whereCondition = " where Apply.Id = @Id ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("Apply", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertApply(Hashtable lstData)
		{
			string sqlQuery ="Insert into Apply (Id, StudentId, UniversityId, AppliedDate) values(@Id, @StudentId, @UniversityId, @AppliedDate);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateApply(Hashtable lstData)
		{
			string sqlQuery = "Update Apply set StudentId = @StudentId, UniversityId = @UniversityId, AppliedDate = @AppliedDate where Apply.Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteApplyById(Hashtable lstData)
		{
			string sqlQuery = "delete from  Apply where Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
