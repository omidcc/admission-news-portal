using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;

namespace Portal.DAL.Base
{
	public class AdmissionCircularDalBase : SqlServerConnection
	{
		public DataTable GetAllAdmissionCircular(Hashtable lstData)
		{
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("AdmissionCircular", "*", "", lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public DataTable GetAdmissionCircularById(Hashtable lstData)
		{
			string whereCondition = " where AdmissionCircular.Id = @Id ";
			DataTable dt = new DataTable();
			try
			{
				dt = GetDataTable("AdmissionCircular", "*", whereCondition, lstData);
				return dt;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		public int InsertAdmissionCircular(Hashtable lstData)
		{
			string sqlQuery ="Insert into AdmissionCircular (UnivId, Circular, year, PostedDate, StartDate, EndDate) values(@UnivId, @Circular, @year, @PostedDate, @StartDate, @EndDate);";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int UpdateAdmissionCircular(Hashtable lstData)
		{
			string sqlQuery = "Update AdmissionCircular set UnivId = @UnivId, Circular = @Circular, year = @year, PostedDate = @PostedDate, StartDate = @StartDate, EndDate = @EndDate where AdmissionCircular.Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}

		public int DeleteAdmissionCircularById(Hashtable lstData)
		{
			string sqlQuery = "delete from  AdmissionCircular where Id = @Id;";
			try
			{
				int success = ExecuteNonQuery(sqlQuery, lstData);
				return success;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
}
