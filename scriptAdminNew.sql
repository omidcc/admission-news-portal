USE [master]
GO
/****** Object:  Database [AdmissionPortalDB]    Script Date: 7/19/2016 3:10:20 PM ******/
CREATE DATABASE [AdmissionPortalDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AdmissionPortalDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSQL\MSSQL\DATA\AdmissionPortalDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'AdmissionPortalDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSQL\MSSQL\DATA\AdmissionPortalDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [AdmissionPortalDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AdmissionPortalDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AdmissionPortalDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [AdmissionPortalDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AdmissionPortalDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AdmissionPortalDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [AdmissionPortalDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AdmissionPortalDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET RECOVERY FULL 
GO
ALTER DATABASE [AdmissionPortalDB] SET  MULTI_USER 
GO
ALTER DATABASE [AdmissionPortalDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AdmissionPortalDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AdmissionPortalDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AdmissionPortalDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'AdmissionPortalDB', N'ON'
GO
USE [AdmissionPortalDB]
GO
/****** Object:  Table [dbo].[AdmissionCircular]    Script Date: 7/19/2016 3:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdmissionCircular](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UnivId] [int] NULL,
	[Circular] [varchar](max) NULL,
	[year] [int] NULL,
	[PostedDate] [date] NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
 CONSTRAINT [PK_AdmissionCircular] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Apply]    Script Date: 7/19/2016 3:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Apply](
	[Id] [int] NOT NULL,
	[StudentId] [int] NULL,
	[UniversityId] [int] NULL,
	[AppliedDate] [date] NULL,
 CONSTRAINT [PK_Apply] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PrivateCost]    Script Date: 7/19/2016 3:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrivateCost](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UnivId] [int] NULL,
	[Cost] [varchar](max) NULL,
 CONSTRAINT [PK_PrivateCost] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Student]    Script Date: 7/19/2016 3:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Student](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[fName] [varchar](255) NULL,
	[mName] [varchar](255) NULL,
	[Dob] [date] NULL,
	[HSCReg] [varchar](20) NULL,
	[HSCRoll] [varchar](20) NULL,
	[HSCCgpa] [decimal](18, 2) NULL,
	[HSCPassingYear] [int] NULL,
	[HSCGroup] [varchar](20) NULL,
	[HSCInstitution] [varchar](500) NULL,
	[HSCBoard] [varchar](20) NULL,
	[SSCReg] [varchar](20) NULL,
	[SSCRoll] [varchar](20) NULL,
	[SSCCgpa] [decimal](18, 2) NULL,
	[SSCPassingYear] [varchar](20) NULL,
	[SSCGroup] [varchar](20) NULL,
	[SSCInstitution] [varchar](500) NULL,
	[SSCBoard] [varchar](20) NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[University]    Script Date: 7/19/2016 3:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[University](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UnivName] [varchar](255) NULL,
	[IsPublic] [bit] NULL,
	[WebLink] [varchar](255) NULL,
	[MinTotal] [decimal](18, 2) NULL,
	[MaxTotal] [decimal](18, 2) NULL,
 CONSTRAINT [PK_University] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 7/19/2016 3:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Role] [varchar](20) NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 7/19/2016 3:10:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](50) NOT NULL,
	[UserRole] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AdmissionCircular] ON 

INSERT [dbo].[AdmissionCircular] ([Id], [UnivId], [Circular], [year], [PostedDate], [StartDate], [EndDate]) VALUES (4, 1, N'NID.pdf', 2016, CAST(0xA03B0B00 AS Date), CAST(0xAA3B0B00 AS Date), CAST(0xB53B0B00 AS Date))
INSERT [dbo].[AdmissionCircular] ([Id], [UnivId], [Circular], [year], [PostedDate], [StartDate], [EndDate]) VALUES (5, 2, N'xi waiting list.pdf', 2016, CAST(0xA03B0B00 AS Date), CAST(0xB13B0B00 AS Date), CAST(0xB53B0B00 AS Date))
INSERT [dbo].[AdmissionCircular] ([Id], [UnivId], [Circular], [year], [PostedDate], [StartDate], [EndDate]) VALUES (6, 1, N'IBA Admission Notice 2015-16.pdf', 2016, CAST(0xA23B0B00 AS Date), CAST(0xA43B0B00 AS Date), CAST(0xE33B0B00 AS Date))
SET IDENTITY_INSERT [dbo].[AdmissionCircular] OFF
INSERT [dbo].[Apply] ([Id], [StudentId], [UniversityId], [AppliedDate]) VALUES (1, 1, 2, CAST(0xA23B0B00 AS Date))
INSERT [dbo].[Apply] ([Id], [StudentId], [UniversityId], [AppliedDate]) VALUES (2, 1, 1, CAST(0xA23B0B00 AS Date))
INSERT [dbo].[Apply] ([Id], [StudentId], [UniversityId], [AppliedDate]) VALUES (3, 1, 1, CAST(0xA23B0B00 AS Date))
INSERT [dbo].[Apply] ([Id], [StudentId], [UniversityId], [AppliedDate]) VALUES (4, 1, 3, CAST(0xA23B0B00 AS Date))
INSERT [dbo].[Apply] ([Id], [StudentId], [UniversityId], [AppliedDate]) VALUES (5, 1, 2, CAST(0xA23B0B00 AS Date))
INSERT [dbo].[Apply] ([Id], [StudentId], [UniversityId], [AppliedDate]) VALUES (6, 1, 1, CAST(0xA23B0B00 AS Date))
INSERT [dbo].[Apply] ([Id], [StudentId], [UniversityId], [AppliedDate]) VALUES (7, 1, 5, CAST(0xA33B0B00 AS Date))
SET IDENTITY_INSERT [dbo].[Student] ON 

INSERT [dbo].[Student] ([Id], [Name], [fName], [mName], [Dob], [HSCReg], [HSCRoll], [HSCCgpa], [HSCPassingYear], [HSCGroup], [HSCInstitution], [HSCBoard], [SSCReg], [SSCRoll], [SSCCgpa], [SSCPassingYear], [SSCGroup], [SSCInstitution], [SSCBoard]) VALUES (1, N'Anupam Dev', N'Rahul Dev', N'Roma Devi', CAST(0xBF180B00 AS Date), N'872542', N'124630', CAST(4.80 AS Decimal(18, 2)), 2010, N'Science', N'Science College', N'Dhaka', N'541278', N'362145', CAST(5.00 AS Decimal(18, 2)), N'2008', N'Science', N'BJ Institution', N'Comilla')
INSERT [dbo].[Student] ([Id], [Name], [fName], [mName], [Dob], [HSCReg], [HSCRoll], [HSCCgpa], [HSCPassingYear], [HSCGroup], [HSCInstitution], [HSCBoard], [SSCReg], [SSCRoll], [SSCCgpa], [SSCPassingYear], [SSCGroup], [SSCInstitution], [SSCBoard]) VALUES (2, N'Khondokar Akash', N'Kh Kash', N'Rahela Akter', CAST(0xED170B00 AS Date), N'345261', N'234413', CAST(3.80 AS Decimal(18, 2)), 2010, N'Science', N'Dhaka College', N'Dhaka', N'145175', N'321456', CAST(4.50 AS Decimal(18, 2)), N'2008', N'Science', N'K.P High School', N'Rajshahi')
INSERT [dbo].[Student] ([Id], [Name], [fName], [mName], [Dob], [HSCReg], [HSCRoll], [HSCCgpa], [HSCPassingYear], [HSCGroup], [HSCInstitution], [HSCBoard], [SSCReg], [SSCRoll], [SSCCgpa], [SSCPassingYear], [SSCGroup], [SSCInstitution], [SSCBoard]) VALUES (3, N'Karim Ullah', N'Hamidullah', N'Rahela Begum', CAST(0xBE160B00 AS Date), N'234516', N'234151', CAST(2.50 AS Decimal(18, 2)), 2010, N'Commerce', N'Dhaka City College', N'Dhaka', N'234151', N'762314', CAST(3.20 AS Decimal(18, 2)), N'2008', N'Science', N'ML School', N'Comilla')
SET IDENTITY_INSERT [dbo].[Student] OFF
SET IDENTITY_INSERT [dbo].[University] ON 

INSERT [dbo].[University] ([Id], [UnivName], [IsPublic], [WebLink], [MinTotal], [MaxTotal]) VALUES (1, N'University Of Dhaka', 1, N'www.du.ac.bd', CAST(8.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[University] ([Id], [UnivName], [IsPublic], [WebLink], [MinTotal], [MaxTotal]) VALUES (2, N'Jahangirnagar University', 1, N'www.ju.ac.bd', CAST(7.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[University] ([Id], [UnivName], [IsPublic], [WebLink], [MinTotal], [MaxTotal]) VALUES (3, N'Khulna University', 1, N'www.ku.edu.bd', CAST(7.50 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[University] ([Id], [UnivName], [IsPublic], [WebLink], [MinTotal], [MaxTotal]) VALUES (4, N'National University of Bangladesh', 1, N'http://www.nu.edu.bd/', CAST(5.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)))
INSERT [dbo].[University] ([Id], [UnivName], [IsPublic], [WebLink], [MinTotal], [MaxTotal]) VALUES (5, N'Jagannath University', 1, N'www.jnu.edu.bd', CAST(6.50 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[University] OFF
SET IDENTITY_INSERT [dbo].[UserRole] ON 

INSERT [dbo].[UserRole] ([Id], [Role]) VALUES (1, N'Admin')
SET IDENTITY_INSERT [dbo].[UserRole] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [Username], [Password], [UserRole]) VALUES (1, N'admin', N'admin', 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[AdmissionCircular]  WITH CHECK ADD  CONSTRAINT [FK_AdmissionCircular_University] FOREIGN KEY([UnivId])
REFERENCES [dbo].[University] ([Id])
GO
ALTER TABLE [dbo].[AdmissionCircular] CHECK CONSTRAINT [FK_AdmissionCircular_University]
GO
ALTER TABLE [dbo].[Apply]  WITH CHECK ADD  CONSTRAINT [FK_Apply_Student] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Student] ([Id])
GO
ALTER TABLE [dbo].[Apply] CHECK CONSTRAINT [FK_Apply_Student]
GO
ALTER TABLE [dbo].[Apply]  WITH CHECK ADD  CONSTRAINT [FK_Apply_University] FOREIGN KEY([UniversityId])
REFERENCES [dbo].[University] ([Id])
GO
ALTER TABLE [dbo].[Apply] CHECK CONSTRAINT [FK_Apply_University]
GO
ALTER TABLE [dbo].[PrivateCost]  WITH CHECK ADD  CONSTRAINT [FK_PrivateCost_PrivateCost] FOREIGN KEY([UnivId])
REFERENCES [dbo].[University] ([Id])
GO
ALTER TABLE [dbo].[PrivateCost] CHECK CONSTRAINT [FK_PrivateCost_PrivateCost]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_UserRole] FOREIGN KEY([UserRole])
REFERENCES [dbo].[UserRole] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_UserRole]
GO
USE [master]
GO
ALTER DATABASE [AdmissionPortalDB] SET  READ_WRITE 
GO
