﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddCircular.aspx.cs" Inherits="Portal.AddCircular" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <link href="css/bootstrap-datepicker.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
   
    <form runat="server" id="form1">
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <section class="form-horizontal">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <center><strong>Create New Admission Circular</strong></center>

                    </div>
                </div>
                <div class="panel-body">
                    <div class="row col-md-12">
                        <div class="form-group">
                            <div class="col-md-3 control-label">University</div>
                            <div class="col-md-9">
                                <asp:DropDownList runat="server" ID="univDropDown" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 control-label">Circular</div>
                            <div class="col-md-3">
                                <asp:FileUpload runat="server" ID="circular" CssClass="btn btn-info" />
                            </div>

                            <div class="col-md-3 control-label">Year</div>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="yearDropDown" CssClass="form-control" />
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="col-md-3 control-label">Application Start</div>
                            <div class="col-md-3">
                                <input type="text" id="startDate" runat="server" class="datepicker form-control" data-provide="datepicker"/>
                            </div>

                            <div class="col-md-3 control-label">Application End</div>
                            <div class="col-md-3">
                                <input type="text" id="endDate" runat="server" class="datepicker form-control" data-provide="datepicker"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-1">
                                <asp:Button runat="server" Text="Save" ID="btnSave" CssClass="btn btn-success" OnClick="btnSave_OnClick" />
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </section>
        
        <script src="Scripts/jquery-1.8.2.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script>
            $(document).ready(function() {
                $('.datepicker').datepicker();
            });
        </script>
    </form>
</asp:Content>
