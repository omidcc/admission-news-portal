﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Portal.BLL;
using Portal.DAL;

namespace Portal
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }

        protected void btnLogin_OnClick(object sender, EventArgs e)
        {
            string username = txtuser.Text;
            string pass = txtpass.Text;

            var user = new Users().GetAllUsers().SingleOrDefault(u => u.Username == username && u.Password == pass);

            if (user != null)
            {
                Session["user"] = user;
                Response.Redirect("ShowCircular.aspx");

            }
            else
            {
                Alert.Show("Invalid username and password");
            }


        }

    }
}