﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StudentApply.aspx.cs" Inherits="Portal.StudentApply" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <form runat="server" id="form1">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <section class="form-horizontal">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <center><strong>Application Area For Student</strong></center>

                    </div>
                </div>
                <div class="panel-body">
                    <div class="row col-md-12">
                        <div id="infoDiv" runat="server" clientidmode="Static">
                            <div class="form-group">
                                <div class="col-md-3 control-label">HSC Roll</div>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtHSCRoll" CssClass="form-control" />
                                </div>
                                <div class="col-md-3 control-label">HSC Passing year</div>
                                <div class="col-md-3">
                                    <asp:DropDownList runat="server" ID="hscYearDropdown" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3 control-label">SSC Roll</div>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtSSCRoll" CssClass="form-control" />
                                </div>
                                <div class="col-md-3 control-label">SSC Passing year</div>
                                <div class="col-md-3">
                                    <asp:DropDownList runat="server" ID="sscYearDropdown" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-1">
                                    <asp:Button runat="server" Text="Submit" ID="btnSubmit" CssClass="btn btn-success" OnClick="btnSubmit_OnClick" />
                                </div>
                            </div>
                        </div>
                        <div id="univDiv" runat="server" clientidmode="Static">
                            <div class="form-group">
                                <div class="col-md-3 control-label">University</div>
                                <div class="col-md-9">
                                    <asp:DropDownList runat="server" ID="univDropdown" CssClass="form-control"/>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-1">
                                    <asp:Button runat="server" Text="Apply" ID="btnApply" CssClass="btn btn-success" OnClick="btnApply_OnClick" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <table runat="server" class="table table-bordered table-responsive" id="myTable" clientidmode="Static">
                            <thead>
                                <tr>
                                    <td colspan="2">
                                        <center><b>Student's Detail Information</b></center>
                                    </td>
                                </tr>
                            </thead>
                            <tr>
                                <td>
                                    <center>Name</center>
                                </td>
                                <td id="stName" runat="server" clientidmode="Static"></td>
                            </tr>
                            <tr>
                                <td>
                                    <center>Father's Name</center>
                                </td>
                                <td id="fName" runat="server" clientidmode="Static"></td>
                            </tr>
                            <tr>
                                <td>
                                    <center>Mother's Name</center>
                                </td>
                                <td id="mName" runat="server" clientidmode="Static"></td>
                            </tr>
                            <tr>
                                <td>
                                    <center>Date Of Birth</center>
                                </td>
                                <td id="dob" runat="server" clientidmode="Static"></td>
                            </tr>
                            <tr>
                                <td>
                                    <center>HSC</center>
                                </td>
                                <td id="hsc" runat="server" clientidmode="Static"></td>
                            </tr>
                            <tr>
                                <td>
                                    <center>SSC</center>
                                </td>
                                <td id="ssc" runat="server" clientidmode="Static"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <script src="Scripts/jquery-1.8.2.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script>
            $(document).ready(function () {
                $('.datepicker').datepicker();
            });
        </script>
    </form>
</asp:Content>

