﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddUnivarsity.aspx.cs" Inherits="Portal.AddUnivarsity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
       <form runat="server" id="form1">
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <section class="form-horizontal">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <center><strong>Create New Admission Circular</strong></center>

                    </div>
                </div>
                <div class="panel-body">
                    <div class="row col-md-12">
                        <div class="form-group">
                            <div class="col-md-3 control-label">University Name</div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtUnivName" CssClass="form-control" />
                            </div>
                        </div>
                       <div class="form-group">
                            <div class="col-md-3 control-label">Website</div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtWeb" CssClass="form-control" />
                            </div>
                        </div>
							 <div class="col-md-3 control-label">Min Total GPA</div>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" ID="txtMin" CssClass="form-control" />
                            </div>
                             <div class="col-md-3 control-label">Max Total GPA</div>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" ID="txtMax" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 control-label">Is Public</div>
                            <div class="col-md-3">
                                <input type="checkbox" id="chkPublic" class="form-control" runat="server"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-1">
                                <asp:Button runat="server" Text="ADD" ID="btnSave" CssClass="btn btn-success" OnClick="btnSave_OnClick" />
                            </div>
                        </div>
                    </div>
                    

                </div>
            
        </section>
        
        <script src="Scripts/jquery-1.8.2.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script>
            $(document).ready(function() {
                $('.datepicker').datepicker();
            });
        </script>
    </form>
</asp:Content>
