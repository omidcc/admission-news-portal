﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Portal.BLL;

namespace Portal
{
    public partial class AddCircular : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                this.LoadUniversity();
                this.LoadYear();
            }
        }

        private void LoadYear()
        {
            List<int>myList=new List<int>();
            for (int i = 2016; i < 2100; i++)
            {
                myList.Add(i);
            }
            yearDropDown.DataSource = myList;
            yearDropDown.DataBind();
        }

        private void LoadUniversity()
        {
           univDropDown.DataSource=new University().GetAllUniversity().ToList();
            univDropDown.DataValueField = "Id";
            univDropDown.DataTextField = "UnivName";
            univDropDown.DataBind();
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                AdmissionCircular obj=new AdmissionCircular();

                obj.Id = obj.GetMaxId() + 1;
                obj.UnivId = Convert.ToInt32(univDropDown.SelectedValue);
                obj.Circular = circular.FileName;
                obj.year = Convert.ToInt32(yearDropDown.SelectedItem.Text);
                obj.PostedDate = DateTime.Now.ToShortDateString();
                obj.StartDate = startDate.Value.ToString();
                obj.EndDate = endDate.Value.ToString();

                if (circular.HasFile)
                {
                    try
                    {
                        string filename = Path.GetFileName(circular.FileName);
                        circular.SaveAs(Server.MapPath("~/circular/") + filename);
                    }
                    catch (Exception ex)
                    {
                        Alert.Show("Upload status: The file could not be uploaded. The following error occured: " + ex.Message);
                    }
                }
                int success = obj.InsertAdmissionCircular();

                string msg = success > 0 ? "Admission Circular Added Successfully" : "Failed!Try again";
                Alert.Show(msg);

            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }
        }
    }
}