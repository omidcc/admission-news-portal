﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Portal.BLL;

namespace Portal
{
    public partial class StudentApply : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.Loadsscyear();
                this.LoadHscYear();
                myTable.Visible = false;
                univDiv.Visible = false;
                
            }
        }

        private void loadUniv()
        {
            decimal totalGpa = Convert.ToDecimal(Session["totalGpa"]);
            List<University>varsityList=new List<University>();
            List<University> varsity = new University().GetAllUniversity().ToList();

            foreach (University v in varsity)
            {
                
                Decimal x = v.MinTotal;
                Decimal y = v.MaxTotal;

                if (totalGpa >= x && totalGpa <= y)
                {
                    varsityList.Add(v);
                }
            }

            univDropdown.DataSource = varsityList;
            univDropdown.DataValueField = "Id";
            univDropdown.DataTextField = "UnivName";
            univDropdown.DataBind();
        }

        private void LoadHscYear()
        {
           List<int>mylist=new List<int>();
            for (int i = 1990; i < 2100; i++)
            {
                mylist.Add(i);
            }
            hscYearDropdown.DataSource = mylist;
            hscYearDropdown.DataBind();
        }

        private void Loadsscyear()
        {

            List<string> mylist = new List<string>();
            for (int i = 1990; i < 2100; i++)
            {
                mylist.Add(i.ToString());
            }
            sscYearDropdown.DataSource = mylist;
            sscYearDropdown.DataBind();
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
            try
            {
                string hscRoll = txtHSCRoll.Text;
                int hscYear = Convert.ToInt32(hscYearDropdown.SelectedItem.Text);
                string sscRoll = txtSSCRoll.Text;
                string sscYear = sscYearDropdown.SelectedItem.Text;
                string studentTable = "";

                var st =
                    new Student().GetAllStudent()
                        .SingleOrDefault(
                            s =>
                                s.HSCRoll == hscRoll && s.HSCPassingYear == hscYear && s.SSCRoll == sscRoll &&
                                s.SSCPassingYear == sscYear);

                if (st == null)
                {
                    Alert.Show("Student not found");
                }
                else
                {
                    myTable.Visible = true;
                    infoDiv.Visible = false;
                    univDiv.Visible = true;
                    this.loadUniv();
                    string stHsc= "HSC Reg No: " + st.HSCReg + "<br>" +
                                    "HSC Roll: " + st.HSCRoll + "<br>HSC Passing Year: " + st.HSCPassingYear + "<br>HSC Institution Year: " + st.HSCInstitution + "<br>HSC Board: " + st.HSCBoard + "<br>HSC CGPA: " + st.HSCCgpa + "";
                    string stSsc= "SSC Reg No: " + st.SSCReg + "<br></center>" +
                                   "SSC Roll: " + st.SSCRoll + "<br>SSC Passing Year: " + st.SSCPassingYear + "<br>SSC Institution Year: " + st.SSCInstitution + "<br>SSC Board: " + st.SSCBoard + "<br>SSC CGPA: " + st.SSCCgpa + "";

                    stName.InnerHtml = st.Name;
                    fName.InnerHtml = st.fName;
                    mName.InnerHtml = st.mName;
                    dob.InnerHtml = st.Dob;
                    hsc.InnerHtml = stHsc;
                    ssc.InnerHtml = stSsc;
                    Session["totalGpa"] = st.HSCCgpa + st.SSCCgpa;
                    this.loadUniv();
                    Session["st"] = st;
                }

            }
            catch (Exception ex)
            {

                Alert.Show(ex.Message);
            }
        }

        protected void btnApply_OnClick(object sender, EventArgs e)
        {
            try
            {
                Apply obj=new Apply();
                obj.Id = obj.GetmaxId() + 1;
                Student st = (Student) Session["st"];
                obj.StudentId = Convert.ToInt32(st.Id);
                obj.UniversityId = Convert.ToInt32(univDropdown.SelectedValue);
                obj.AppliedDate = DateTime.Now.ToShortDateString();
                
                int success = obj.InsertApply();

                string msg = success > 0 ? "Application submitted" : "failed!try again";
                Alert.Show(msg);
             
                    Response.Redirect("StudentApply.aspx");
                Session["st"] = null;

            }
            catch (Exception ex)
            {
                
               Alert.Show(ex.Message);
            }
        }
    }
}