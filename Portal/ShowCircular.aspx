﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShowCircular.aspx.cs" Inherits="Portal.ShowCircular" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <form runat="server" id="form1">
        <div class="row">
            <div class="col-md-3 box-body">
                <asp:Button runat="server" CssClass="btn btn-warning" ID="btnAddNew" Text="Add New Circular" OnClick="btnAddNew_OnClick"/>
                <asp:Button runat="server" CssClass="btn btn-primary" ID="btnAddVarsity" Text="Add University" OnClick="btnAddVarsity_OnClick"/>
            </div>            
        </div>
        <section id="secPanel" runat="server">
        </section>
    </form>
    <script src="Scripts/jquery-1.8.2.js"></script>
    <script>
        var pdfAnchors = document.querySelectorAll("a[href$='.pdf']");

        for (var i = 0; i < pdfAnchors.length; i++) {
            pdfAnchors[i].target = pdfAnchors[i].target || "_new";
        }


        for (var i = 0; i < document.links.length ; i++) {
            var link = document.links[i];
            var linkHost = link.href.split("/")[2];
            if (linkHost !== window.location.host) {
                link.target = link.target || "_external";
            }
        }

    </script>
</asp:Content>
