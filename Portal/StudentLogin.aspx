﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StudentLogin.aspx.cs" Inherits="Portal.StudentLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <form runat="server" id="form1">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <section class="form-horizontal">
            <div class="panel panel-success" style="padding: 100px 200px 200px 200px;">
                <div class="panel-heading">
                    <div class="panel-title">
                        <center><strong>Student Login</strong></center>

                    </div>
                </div>
                <div class="panel-body">

                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-3 control-label">HSC Roll</div>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" ID="txtHSCRoll" CssClass="form-control" />
                            </div>
                            <div class="col-md-3 control-label">HSC Passing year</div>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="hscYearDropdown" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 control-label">SSC Roll</div>
                            <div class="col-md-3">
                                <asp:TextBox runat="server" ID="txtSSCRoll" CssClass="form-control" />
                            </div>
                            <div class="col-md-3 control-label">SSC Passing year</div>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="sscYearDropdown" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-1">
                                <asp:Button runat="server" Text="Login" ID="btnSubmit" CssClass="btn btn-success" OnClick="btnSubmit_OnClick" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


    </form>
</asp:Content>
