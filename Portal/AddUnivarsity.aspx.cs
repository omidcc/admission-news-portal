﻿using Portal.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portal
{
    public partial class AddUnivarsity : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                University obj = new University();
                obj.Id = obj.GetMaxId() + 1;
                obj.UnivName = txtUnivName.Text;
                obj.IsPublic = chkPublic.Checked;
                obj.WebLink = txtWeb.Text;

                obj.MaxTotal = Convert.ToDecimal(txtMax.Text);
                obj.MinTotal = Convert.ToDecimal(txtMin.Text);
                
                int success = obj.InsertUniversity();
                string msg = success > 0 ? "University Added Successfully" : "Failed";
                Alert.Show(msg);
                txtUnivName.Text = "";
                txtWeb.Text = "";
            }
            catch (Exception ex)
            {
                Alert.Show(ex.Message);
            }
        }
    }
}