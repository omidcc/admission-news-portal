﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Portal.BLL;

namespace Portal
{
    public partial class ShowCircular : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                btnAddNew.Visible = false;
                btnAddVarsity.Visible = false;
            }
            if (!IsPostBack)
            {

                this.LoadCircularPanels();
            }
        }

        private void LoadCircularPanels()
        {
            var circulars=new AdmissionCircular().GetAllAdmissionCircular().ToList();
            string panel = "";
            foreach (var circular in circulars)
            {
                var univ=new University().GetUniversityById(circular.UnivId);
                string source = "/circular/" + circular.Circular;
                panel +=
                    " <div class='panel panel-info'><div class='panel-heading'><div class='panel-title'> <center><strong>" +
                    univ.UnivName +
                    "</strong></center> </div></div><div class='panel-body'> <div class='col-md-12'><div class='col-md-6'> <div class='form-group'><b>Posted On: </b>" +
                    circular.PostedDate + "<br><b>Application Start: </b>" + circular.StartDate +
                    "<br><b>Application End: </b>" + circular.EndDate +
                    "<br></div> </div> <div class='col-md-6'><iframe src='"+source+"'  id='dwnfile' frameborder='0' width='200' height='100'></iframe><br><a href='"+source+"' target='dwnfile'>Download</a></div></div></div></div>";
               
            }
            secPanel.InnerHtml = panel;
        }

        protected void btnAddNew_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("AddCircular.aspx");
        }
        protected void btnAddVarsity_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("AddUnivarsity.aspx");
        }
    }
}
//<button id='btnDownload' runat='server' type='submit'class='btn btn-primary'>Download</button>