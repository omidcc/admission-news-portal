﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portal
{
    public partial class StudentLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                this.Loadsscyear();
                this.LoadHscYear();
            }
        }
        private void LoadHscYear()
        {
            List<int> mylist = new List<int>();
            for (int i = 1990; i < 2100; i++)
            {
                mylist.Add(i);
            }
            hscYearDropdown.DataSource = mylist;
            hscYearDropdown.DataBind();
        }

        private void Loadsscyear()
        {

            List<string> mylist = new List<string>();
            for (int i = 1990; i < 2100; i++)
            {
                mylist.Add(i.ToString());
            }
            sscYearDropdown.DataSource = mylist;
            sscYearDropdown.DataBind();
        }

        protected void btnSubmit_OnClick(object sender, EventArgs e)
        {
           
        }
    }
}