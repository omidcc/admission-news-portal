﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Portal.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    
    <form runat="server" id="form1">
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <section class="form-horizontal">
            <div class="panel panel-success" style="padding: 100px 200px 200px 200px;">
                <div class="panel-heading">
                    <div class="panel-title">
                        <center><strong>Login</strong></center>

                    </div>
                </div>
                <div class="panel-body">
                   
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-3 control-label">Username</div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtuser" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 control-label">Password</div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtpass" TextMode="Password" CssClass="form-control" />
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-1">
                                <asp:Button runat="server" Text="Login" ID="btnLogin" CssClass="btn btn-success" OnClick="btnLogin_OnClick" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        
       
    </form>

</asp:Content>
